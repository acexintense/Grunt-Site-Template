module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
		    js: {
		    	src: ['javascript/*.js'],
		    	dest: 'build/js/main.js',
		    	nonull: true,
		    },
			css: {
		    	src: ['stylesheets/*.css'],
		    	dest: 'build/css/main.css',
		    	nonull: true,
		    },
		},
		watch: {
			check: {
				files: [ '**/*'],
				tasks: ['copy', 'concat', 'notify'],
			},
		},
		copy: {
			main: {
			   	src: ['*.html', '*.php'],
		    	dest: 'build/',
	        },
		},
		notify: {
		    task: {
		      options: {
		        title: 'Task Complete',  // optional 
		        message: 'Files built successfully!' //required 
		      },
		    },
		  },
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-notify');
	grunt.registerTask('default', 'watch');
};